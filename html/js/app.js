angular.module('shubhaKalyaan', ['ngRoute', 'ui.bootstrap'])
    .config(function($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'components/appHomepage.html',
            controller: 'appHomeController'
        }).when('/registration', {
            templateUrl: 'components/basicRegistration/basic-registration.html'
        }).when('/preferences', {
            templateUrl: 'components/partner-preference/partner-preference.html'
        }).when('/profilePreview', {
            templateUrl: 'components/profile-preview/profile-preview.html'
        }).when('/otherProfiles', {
            templateUrl: 'components/other-profile/other-profile.html'
        }).when('/profile', {
            templateUrl: 'components/profile/myprofile.html'
        }).when('/uploadPhotos', {
            templateUrl: 'components/upload-photos/upload-photos.html'
        }).when('/verifyPhoneNumber', {
            templateUrl: 'components/verify-phone-number/verify-number.html'
        })
    });